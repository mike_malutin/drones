#include "MaxAlgo.h"
#include "MikeAlgo.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>

#include <cassert>
#include <algorithm>

using namespace std;
struct Point
{
  int x;
  int y;
};

istream& operator >> ( istream& is, Point& p ) {
  is >> p.x;
  is >> p.y;
  return is;
}

int rows = 0;
int cols = 0;
int n_drones = 0;
int max_turns = 0;
int max_payload = 0;

const int MAX_N_PRODUCTS = 2000;
int product_weight[MAX_N_PRODUCTS] = {};
int n_products = 0;

const int MAX_N_WAREHOUSES = 20;
int n_warehouses = 0;
Point warehouse_pos[MAX_N_WAREHOUSES] = {};
int warehouse_store[MAX_N_WAREHOUSES][MAX_N_PRODUCTS] = {};

const int MAX_N_ORDERS = 2000;
int n_orders = 0;
Point order_pos[MAX_N_ORDERS] = {};
map<int, int> order[MAX_N_ORDERS];

vector<Point> drone_pos;
vector<int> drone_free_time;
int turn = 0;


struct Command
{
  int drone;
  char command;
  // L
  struct
  {
    int warehouse;
    int product;
    int n_products;
  } load;

  // D
  struct
  {
    int order;
    int product;
    int n_products;
  } deliver;

  // W
  struct
  {
    int turns;
  } wait;

};

void init( istream& is ) {
  is >> rows;
  is >> cols;
  is >> n_drones;
  is >> max_turns;
  is >> max_payload;

  is >> n_products;
  for ( int n = 0; n < n_products; ++n ) {
    is >> product_weight[n];
  }

  is >> n_warehouses;
  for ( int w = 0; w < n_warehouses; ++w ) {
    is >> warehouse_pos[w];
    for ( int p = 0; p < n_products; ++p ) {
      is >> warehouse_store[w][p];
    }
  }

  is >> n_orders;
  for ( int o = 0; o < n_orders; ++o ) {
    is >> order_pos[o];
    int n_items;
    is >> n_items;
    for ( int i = 0; i < n_items; ++i ) {
      int product;
      is >> product;
      ++order[o][product];
    }
  }

  drone_pos.resize( n_drones, warehouse_pos[0] );
  drone_free_time.resize( n_drones, 0 );
}

Command loadCommand(int drone, int load_warehouse, int load_product, int load_n_product)
{
    Command c = { drone,'L',{ load_warehouse ,load_product,load_n_product },{},{} };
    return c;
}

Command deliveryCommand(int drone, int dOrder, int dProduct, int dNProducts)
{
    Command c = { drone,'D',{},{ dOrder , dProduct ,dNProducts },{} };
    return c;
}

Command waitCommand(int drone, int wTurns)
{
    Command c = { drone,'W',{},{},{ wTurns } };
    return c;
}

int sqr( int v ) {
  return v * v;
}

int squared_distance( const Point& from, const Point& to ) {
  return sqr( to.x - from.x ) + sqr( to.y - from.y );
}

int distance( const Point& from, const Point& to ) {
  return (int)(ceil(sqrt((double)sqr( to.x - from.x ) + (double)sqr( to.y - from.y ))) + 0.5);
}

tuple<double, vector<int>> load_and_deliver( vector<Command>& commands, int drone, int order );

// ������� ���������� ���������� ����� ������� ������� ����� �������� ����� ��� ��������� �������
tuple<int, double> find_free_optimal_drone( int turn, int order ) {
  static vector<int> products;
  static vector<Command> commands;
  double best_cost = std::numeric_limits<double>::max();
  int best_drone = -1;
  for ( int d = 0; d < n_drones; ++d ) {
    if ( drone_free_time[d] <= turn ) {
      double time;
      commands.clear();
      tie( time, products ) = load_and_deliver( commands, d, order );
      assert( !products.empty() );
      double cost = time / products.size();
      if ( cost < best_cost ) {
        best_cost = cost;
        best_drone = d;
      }
    }
  }
  return make_tuple(best_drone, best_cost);
}

// Add two commands: Load and Deliver as much as can products for order

// Add two commands: Load and Deliver as much as can products for order
tuple<double, vector<int>> load_and_deliver(vector<Command>& commands, int drone, int order) {

    auto& order_ = ::order[order];

    int best_warehouse = -1;
    int max_load = 0;
    map<int, int> best_products;

    for (int i = 0; i < n_warehouses; ++i)
    {
        int load = 0;
        map<int, int> products;

        for (auto& it : order_)
        {
            auto pid = it.first;
            int n = it.second;

            if (warehouse_store[i][pid] > 0)
            {
                int new_load = load;
                int n_prods = min(warehouse_store[i][pid], 1);
                new_load += n_prods * product_weight[pid];

                if (new_load > max_payload)
                {
                    break;
                }

                products[pid] += n_prods;
                load += new_load;
            }
        }

        if (load > max_load)
        {
            best_warehouse = i;
            max_load = load;
            best_products = products;
        }
    }

    double cost = 0;
    vector<int> prods;

    if (best_warehouse > -1)
    {
      {
        cost += distance( drone_pos[drone], warehouse_pos[best_warehouse] );
        Command c;
        c.command = 'L';
        c.drone = drone;
        c.load.warehouse = best_warehouse;


        for ( auto& it : best_products ) {
          c.load.product = it.first;
          c.load.n_products = it.second;
          commands.push_back( c );
          cost += 1;

          for ( int i = 0; i < it.second; ++i ) {
            prods.push_back( it.first );
          }
        }
      }
      {
        cost += distance( warehouse_pos[best_warehouse], order_pos[order] );
        Command c;
        c.command = 'D';
        c.drone = drone;
        c.deliver.order = order;

        for ( auto& it : best_products ) {
          c.deliver.product = it.first;
          c.deliver.n_products = it.second;
          commands.push_back( c );
          cost += 1;
        }
      }
    }

    return make_tuple(cost, prods);
}

int command_turns( const Command& c ) {
    int result = 0;
    switch (c.command)
    {
    case 'L':
        result = distance(drone_pos[c.drone], warehouse_pos[c.load.warehouse]) + 1;
        break;
    case 'D':
        result = distance(drone_pos[c.drone], order_pos[c.deliver.order]) + 1;
        break;
    case 'W':
        result = c.wait.turns;
        break;
    default:
        assert(false);
        break;
    }

  return result;
}

tuple<int, vector<Command>> run() {
  vector<Command> commands;
  int score = 0;
  for ( int turn = 0; turn < max_turns; ++turn ) {
    // Find best drone and order
    int best_order = -1;
    int best_drone = -1;
    double best_cost = numeric_limits<double>::max();
    bool all_delivered = true;
    for ( int o = 0; o < n_orders; ++o ) {
      if ( !order[o].empty() ) {
        all_delivered = false;
        int drone;
        double cost;
        tie( drone, cost ) = find_free_optimal_drone( turn, o );
        if ( cost < best_cost ) {
          best_cost = cost;
          best_drone = drone;
          best_order = o;
        }
      }
    }
    if ( best_drone != -1 ) {
      vector<int> products;
      double time;
      size_t prev_n_commands = commands.size();
      tie( time, products ) = load_and_deliver( commands, best_drone, best_order );
      assert( !products.empty() );
      assert( commands.back().command == 'D' );
      for ( size_t c = prev_n_commands; c < commands.size(); ++c ) {
        const auto& command = commands[c];
        if ( command.command == 'L' ) {
          const int w = command.load.warehouse;
          assert( w < n_warehouses );
          assert( warehouse_store[w][command.load.product] >= command.load.n_products );
          warehouse_store[w][command.load.product] -= command.load.n_products;
        }
      }
      drone_free_time[best_drone] = turn + int(time + 0.5);
      drone_pos[best_drone] = order_pos[commands.back().deliver.order];
      for ( int p : products ) {
        assert( order[best_order][p] > 0 );
        if ( --order[best_order][p] == 0 ) {
          order[best_order].erase( p );
          if ( order[best_order].empty() ) {
            score += int(floor(100 * (max_turns - turn) / max_turns) + 0.5);
//            order[best_order] = std::move( order[n_orders--] );
          }
        }
      }
    }
    if ( all_delivered )
      break;
  }
  return make_tuple(score, commands);
}

int main()
{
  const string fname = "redundancy";
  //const string fname = "mother_of_all_warehouses";
  //const string fname = "example";
  std::ifstream is( fname + ".in" );
  init( is );

  int score;
  vector<Command> commands;
  tie( score, commands ) = run();

  std::ofstream os( fname + ".out" );
  // Store commands
  if (os.is_open() && !commands.empty())
  {
      os << commands.size() << "\n";
      
      for (const auto & command : commands)
      {
      switch(command.command)
      {
        case 'L':
            os << command.drone << " "
                << command.command << " "
                << command.load.warehouse << " "
                << command.load.product << " "
                << command.load.n_products << "\n";
        break;
        case 'D':
            os << command.drone << " "
                << command.command << " "
                << command.deliver.order << " "
                << command.deliver.product << " "
                << command.deliver.n_products << "\n";
        break;
        case 'W':
            os << command.drone << " "
                << command.command << " "
                << command.wait.turns << "\n";
            break;
        default:
        assert(false);
        break;
      }
              
      }
  os.close();
  }
  return 0;
}